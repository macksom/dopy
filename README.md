# Dopy
Dopy is a clipboard helper which copies the whole file
## Installation
```bash
cargo build --release
```
or for debug mode
```bash
cargo build
```
## Contributing

Pull requests are welcome. For major changes, please open an issue first
to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
Check LICENSE