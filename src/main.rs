use arboard::Clipboard;
use std::env;
use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use std::process::exit;
fn help() {
    println!("Options: --help or -h; --version or -v; Only utf-8 files are supported");
}
fn version() {
    let version = env!("CARGO_PKG_VERSION");
    println!("Dopy version {}", version);
}
//Check if there is a file path in arguments
fn check_for_files(args: &Vec<String>) {
    if args.len() == 1 {
        panic!("No files are provided");
    }
}
fn clear_clipboard(clipboard: &mut Clipboard) {
    match clipboard.clear(){
        Ok(_) => (),
        Err(_) => panic!("Unable to clear the clipboard"),
    }
}
/*Check for file path in arguments: --version example.txt, where example.txt is a path */
fn handle_args(args: &Vec<String>, file_path: &mut String) {
    check_for_files(args);
    for arg in &args[1..] {
        if arg.starts_with("--") || arg.starts_with("-") {
            match arg.as_str() {
                "--help" | "-h" => {
                    help();
                    exit(0);
                }
                "--version" | "-v" => {
                    version();
                    exit(0);
                }
                _ => panic!("Invalid argument"),
            }
        } else {
            *file_path = arg.trim().to_string();
            return;
        }
    }
}
/* Creating new file */
fn new_file(file_path: &Path) -> File {
    let file = match File::open(&file_path) {
        Err(why) => panic!("Couldn't open file: {}", why),
        Ok(file) => file,
    };
    file
}
/* Copy the file context to the clipboard */
fn copy(mut file: File, mut clipboard: Clipboard) {
    let mut file_text = String::new();
    match file.read_to_string(&mut file_text) {
        Err(why) => panic!("Couldn't read file: {}", why),
        Ok(_) => {
            clipboard.set_text(file_text.trim().to_owned()).unwrap();
            println!("Succesfully copied!");
        }
    }
}
fn main() {
    let args: Vec<String> = env::args().collect();
    let mut clipboard = Clipboard::new().unwrap();
    let mut file_path: String = String::new();
    handle_args(&args, &mut file_path);
    let file_path = Path::new(file_path.as_str());
    let file = new_file(file_path);
    clear_clipboard(&mut clipboard);
    copy(file, clipboard);
}
